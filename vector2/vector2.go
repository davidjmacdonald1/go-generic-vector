package vector2

import (
	"fmt"
	"math"

	"golang.org/x/exp/constraints"
)

// Num represents any real number type.
// Use this to make generic functions that return generic Vector2s.
type Num interface {
	constraints.Integer | constraints.Float
}

// Vector2 represents an immutable 2D vector of any real number type.
type Vector2[T Num] struct {
	x, y T
}

// Make returns a vector of type T with components x and y.
func Make[T Num](x, y T) Vector2[T] {
	return Vector2[T]{x: x, y: y}
}

// MakeAtAngle returns a vector of type T with the given magnitude
// at the given angle in radians.
func MakeAtAngle[T Num](magnitude, angle float64) Vector2[T] {
	v := Make(math.Cos(angle), math.Sin(angle))
	return To[T](v.Scale(magnitude))
}

// To returns the result of casting a vector of type E to type T.
func To[T, E Num](v Vector2[E]) Vector2[T] {
	return Make(T(v.x), T(v.y))
}

// X returns the x component.
func (v Vector2[T]) X() T {
	return v.x
}

// Y returns the y component.
func (v Vector2[T]) Y() T {
	return v.y
}

// XY returns both the x and y component.
func (v Vector2[T]) XY() (x, y T) {
	return v.x, v.y
}

// Add will add this and v2 together.
// A new vector with the result will be returned.
func (v Vector2[T]) Add(v2 Vector2[T]) Vector2[T] {
	return Make(v.x+v2.x, v.y+v2.y)
}

// AddX will add x into the x component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddX(x T) Vector2[T] {
	return Make(v.x+x, v.y)
}

// AddY will add y into the y component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddY(y T) Vector2[T] {
	return Make(v.x, v.y+y)
}

// AddXY will add x and y to their respective component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddXY(x, y T) Vector2[T] {
	return Make(v.x+x, v.y+y)
}

// Sub will subtract v2 from this.
// A new vector with the result will be returned.
func (v Vector2[T]) Sub(v2 Vector2[T]) Vector2[T] {
	return Make(v.x-v2.x, v.y-v2.y)
}

// SubX will subtract x from the x component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubX(x T) Vector2[T] {
	return Make(v.x-x, v.y)
}

// SubY will subtract y from the y component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubY(y T) Vector2[T] {
	return Make(v.x, v.y-y)
}

// SubXY will subtract x and y from their respective component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubXY(x, y T) Vector2[T] {
	return Make(v.x-x, v.y-y)
}

// Dot will multiply this by v2 using dot product multiplication.
// The result will be returned.
func (v Vector2[T]) Dot(v2 Vector2[T]) T {
	return v.x*v2.x + v.y*v2.y
}

// Cross will multiply this by v2 using cross product multiplication.
// The result will be returned.
func (v Vector2[T]) Cross(v2 Vector2[T]) T {
	return v.x*v2.y - v.y*v2.x
}

// Scale will multiply this by a scalar.
// A new vector with the result will be returned.
func (v Vector2[T]) Scale(a T) Vector2[T] {
	return Make(v.x*a, v.y*a)
}

// MulX will multiply the x component by x.
// A new vector with the result will be returned.
func (v Vector2[T]) MulX(x T) Vector2[T] {
	return Make(v.x*x, v.y)
}

// MulY will multiply the y component by y.
// A new vector with the result will be returned.
func (v Vector2[T]) MulY(y T) Vector2[T] {
	return Make(v.x, v.y*y)
}

// MulXY will multiply the x and y components by their respective parameter.
// A new vector with the result will be returned.
func (v Vector2[T]) MulXY(x, y T) Vector2[T] {
	return Make(v.x*x, v.y*y)
}

// InvScale will divide this by a scalar.
// A new vector with the result will be returned.
func (v Vector2[T]) InvScale(a T) Vector2[T] {
	return Make(v.x/a, v.y/a)
}

// DivX will divide the x component by x.
// A new vector with the result will be returned.
func (v Vector2[T]) DivX(x T) Vector2[T] {
	return Make(v.x/x, v.y)
}

// DivY will divide the y component by y.
// A new vector with the result will be returned.
func (v Vector2[T]) DivY(y T) Vector2[T] {
	return Make(v.x, v.y/y)
}

// DivXY will divide the x and y components by their respective parameter.
// A new vector with the result will be returned.
func (v Vector2[T]) DivXY(x, y T) Vector2[T] {
	return Make(v.x/x, v.y/y)
}

// Normalize will calculate the unit vector for this.
// A new vector with the result will be returned.
func (v Vector2[T]) Normalize() Vector2[T] {
	u := To[float64](v)
	return To[T](u.InvScale(u.Length()))
}

// Length will calculate the length of this.
// The result will be returned.
func (v Vector2[T]) Length() float64 {
	return math.Sqrt(float64(v.x*v.x + v.y*v.y))
}

// Angle will calculate the angle of this off of the unit circle.
// The result will be returned in radians.
func (v Vector2[T]) Angle() float64 {
	return math.Atan2(float64(v.y), float64(v.x))
}

// Distance will calculate the distance from this to v2.
// The result will be returned.
func (v Vector2[T]) Distance(v2 Vector2[T]) float64 {
	return v.Sub(v2).Length()
}

// Lerp will perform linear interpolation between this and v2 using factor t.
// A new vector with the result will be returned.
func (v Vector2[T]) Lerp(v2 Vector2[T], t T) Vector2[T] {
	return v.Add(v2.Sub(v).Scale(t))
}

// Project will project this onto v2.
// A new vector with the result will be returned.
func (v Vector2[T]) Project(v2 Vector2[T]) Vector2[T] {
	return v2.Scale(v.Dot(v2)).InvScale(v2.Dot(v2))
}

// Reflect will reflect this over v2.
// A new vector with the vv2esult will be returned.
func (v Vector2[T]) Reflect(v2 Vector2[T]) Vector2[T] {
	return v.Sub(v.Project(v2).Scale(2))
}

// Rotate will rotate this around the unit circle by angle radians.
// A new vector with the result will be returned.
func (v Vector2[T]) Rotate(angle float64) Vector2[T] {
	x, y := To[float64](v).XY()
	cos, sin := math.Cos(angle), math.Sin(angle)
	return Make(T(cos*x-sin*y), T(sin*x+cos*y))
}

// Apply will apply a function to both components individually.
// A new vector with the result will be returned.
func (v Vector2[T]) Apply(f func(T) T) Vector2[T] {
	return Make(f(v.x), f(v.y))
}

// Map will apply a function to both components together.
// A new vector with the result will be returned.
func (v Vector2[T]) Map(f func(T, T) (T, T)) Vector2[T] {
	return Make(f(v.x, v.y))
}

// String will return the vector formatted as a string.
func (v Vector2[T]) String() string {
	return fmt.Sprintf("<%v, %v>", v.x, v.y)
}
