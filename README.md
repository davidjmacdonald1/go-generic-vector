# Go Generic Vector
A vector library for 2D applications.

This is a generic version of [this](https://github.com/deeean/go-vector) library.
I was tired of casting my vectors from `float64` to `float32`.

I have made some different decisions than the original;
for instance, I hid the internal `x` and `y` values to enforce immutability.
I also use copies rather than pointers.

## Installation
```go
import "gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
```

Then, run either
```shell
go get -u gitlab.com/davidjmacdonald1/go-generic-vector/vector2
```
or
```shell
go mod tidy
```

## Usage
```go
package main

import (
    "fmt"
    "math"

    "gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
)

func main() {
    // Non-Generic Usage
    pos := vector2.Make(0.0, 0.0)
    vel := vector2.MakeAtAngle[float64](100, math.Pi/4)
    acc := vector2.Make(0, -9.81)
    t := 12.0

    pos = pos.Add(vel.Scale(t).Add(acc.Scale(t*t/2))) // xf = xi + vt + .5at^2
    fmt.Printf("After %.1fs, with velocity %v, it reached %v\n", t, vel, pos)

    // Generic Usage
    point := vector2.Make(2, 5) // Vector[int]
    x, y := vector2.To[float64](point).XY() // Casting using vector2.To
    fmt.Println(math.Abs(x + y)) // Values are now float64

    // Higher order functions
    nums := vector2.Make(9.0, 16.0)
    sqrts := nums.Apply(math.Sqrt) // Get the sqrt of each component
    sqrts = sqrts.Map(func(x, y float64) (float64, float64) {
        return -x, math.Log2(y) // Apply different functions
    })
    fmt.Println(sqrts.XY())
}
```

## Documentation

### Vector2
```go
// Num represents any real number type.
// Use this to make generic functions that return generic Vector2s.
type Num interface

// Vector2 represents an immutable 2D vector of any real number type.
type Vector2[T Num] struct

// Make returns a vector of type T with components x and y.
func Make[T Num](x, y T) Vector2[T]

// MakeAtAngle returns a vector of type T with the given magnitude
// at the given angle in radians.
func MakeAtAngle[T Num](magnitude, angle float64) Vector2[T]

// To returns the result of casting a vector of type E to type T.
func To[T, E Num](v Vector2[E]) Vector2[T]

// X returns the x component.
func (v Vector2[T]) X() T

// Y returns the y component.
func (v Vector2[T]) Y() T

// XY returns both the x and y component.
func (v Vector2[T]) XY() (x, y T)

// Add will add this and v2 together.
// A new vector with the result will be returned.
func (v Vector2[T]) Add(v2 Vector2[T]) Vector2[T]

// AddX will add x into the x component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddX(x T) Vector2[T]

// AddY will add y into the y component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddY(y T) Vector2[T]

// AddXY will add x and y to their respective component.
// A new vector with the result will be returned.
func (v Vector2[T]) AddXY(x, y T) Vector2[T]

// Sub will subtract v2 from this.
// A new vector with the result will be returned.
func (v Vector2[T]) Sub(v2 Vector2[T]) Vector2[T]

// SubX will subtract x from the x component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubX(x T) Vector2[T]

// SubY will subtract y from the y component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubY(y T) Vector2[T]

// SubXY will subtract x and y from their respective component.
// A new vector with the result will be returned.
func (v Vector2[T]) SubXY(x, y T) Vector2[T]

// Dot will multiply this by v2 using dot product multiplication.
// The result will be returned.
func (v Vector2[T]) Dot(v2 Vector2[T]) T

// Cross will multiply this by v2 using cross product multiplication.
// The result will be returned.
func (v Vector2[T]) Cross(v2 Vector2[T]) T

// Scale will multiply this by a scalar.
// A new vector with the result will be returned.
func (v Vector2[T]) Scale(a T) Vector2[T]

// MulX will multiply the x component by x.
// A new vector with the result will be returned.
func (v Vector2[T]) MulX(x T) Vector2[T]

// MulY will multiply the y component by y.
// A new vector with the result will be returned.
func (v Vector2[T]) MulY(y T) Vector2[T]

// MulXY will multiply the x and y components by their respective parameter.
// A new vector with the result will be returned.
func (v Vector2[T]) MulXY(x, y T) Vector2[T]

// InvScale will divide this by a scalar.
// A new vector with the result will be returned.
func (v Vector2[T]) InvScale(a T) Vector2[T]

// DivX will divide the x component by x.
// A new vector with the result will be returned.
func (v Vector2[T]) DivX(x T) Vector2[T]

// DivY will divide the y component by y.
// A new vector with the result will be returned.
func (v Vector2[T]) DivY(y T) Vector2[T]

// DivXY will divide the x and y components by their respective parameter.
// A new vector with the result will be returned.
func (v Vector2[T]) DivXY(x, y T) Vector2[T]

// Normalize will calculate the unit vector for this.
// A new vector with the result will be returned.
func (v Vector2[T]) Normalize() Vector2[T]

// Length will calculate the length of this.
// The result will be returned.
func (v Vector2[T]) Length() float64

// Angle will calculate the angle of this off of the unit circle.
// The result will be returned in radians.
func (v Vector2[T]) Angle() float64

// Distance will calculate the distance from this to v2.
// The result will be returned.
func (v Vector2[T]) Distance(v2 Vector2[T]) float64

// Lerp will perform linear interpolation between this and v2 using factor t.
// A new vector with the result will be returned.
func (v Vector2[T]) Lerp(v2 Vector2[T], t T) Vector2[T]

// Project will project this onto v2.
// A new vector with the result will be returned.
func (v Vector2[T]) Project(v2 Vector2[T]) Vector2[T]

// Reflect will reflect this over v2.
// A new vector with the vv2esult will be returned.
func (v Vector2[T]) Reflect(v2 Vector2[T]) Vector2[T]

// Rotate will rotate this around the unit circle by angle radians.
// A new vector with the result will be returned.
func (v Vector2[T]) Rotate(angle float64) Vector2[T]

// Apply will apply a function to both components individually.
// A new vector with the result will be returned.
func (v Vector2[T]) Apply(f func(T) T) Vector2[T]

// Map will apply a function to both components together.
// A new vector with the result will be returned.
func (v Vector2[T]) Map(f func(T, T) (T, T)) Vector2[T]

// String will return the vector formatted as a string.
func (v Vector2[T]) String() string
```
